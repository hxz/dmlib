
(in-package :orderable)

;; --------------------------------------

;;(defvar *order-count* 0)
(defvar *order-count* (list 0)) ; by hxz, 20211229

(defclass <orderable-mixin> ()
  ;; multilocks must be acquired in total order
  ;; to prevent deadlocks
  ((id   :reader order-id
         ;;:initform (sys:atomic-fixnum-incf *order-count*))
         :initform (sb-ext:atomic-incf (car *order-count*))) ; by hxz, 20211229
   ))

(defmethod ord:compare ((a <orderable-mixin>) (b <orderable-mixin>))
  (- (order-id a) (order-id b)))
