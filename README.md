
```
David McClain's actors library in Emotiq <https://github.com/stegos/Chat>, and <https://github.com/dbmcclain/Lisp-Actors>.
Try to port this awesome library or even enormous system to SBCL and Clozure CL.

The version 1.0, based on <https://github.com/stegos/Chat>, compile passed in sbcl and ccl.
The version 2.0, based on <https://github.com/dbmcclain/Lisp-Actors/Actors>, compile passed in sbcl.
The version 3.0, based on <https://github.com/dbmcclain/Lisp-Actors/xTActors>, compile failed.

The interfaces changed a lot since version 2.0, and widely utilized functions in lispworks, and thus the porting failed.

Please note that all these were not tested, and there should be errors anywhere.

Actors: actors version 2.0
cps: compile passed
Crypto: compile failed
data-objects: compile passed
lisp-object-encoder: gave up
mpcompat: compile passed
sdle-store: compile passed
useful-macros: compile passed
xTActors: compile failed
versions: the tars of each versions, currently only for version 1.0, which corresponds to the last commition to gitee <https://gitee.com/hxz/dmlib> in 2021-12-29.

```
